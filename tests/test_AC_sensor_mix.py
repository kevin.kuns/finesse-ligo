# %%
import finesse.ligo.lho
import finesse.ligo.asc as asc
from finesse.components import ZPKFilter
from finesse.ligo.suspension import QUADSuspension
import sys

# %%
lho = finesse.ligo.lho.make_O4_lho(
    add_quads=True, quad_model=QUADSuspension, quad_kwargs={}
)
lho.modes("even", maxtem=4)
finesse.ligo.lho.add_AS_WFS(lho)
asc.add_arm_ASC_DOFs(lho)

DHARD_P_ctrl = lho.add(ZPKFilter("DHARD_P_ctrl", [], [], 1))

# Connect up two y sensors and give them some names to define the input
# matrix gains
lho.connect(lho.AS_A_WFS45y.I, DHARD_P_ctrl.p1, name="INP_AS_A_WFS45y")
lho.connect(lho.AS_B_WFS45y.I, DHARD_P_ctrl.p1, name="INP_AS_B_WFS45y")
lho.INP_AS_A_WFS45y.gain = 1
lho.INP_AS_B_WFS45y.gain = -0.5
# connect the controller to DHARD_P input
lho.connect(DHARD_P_ctrl.p2, lho.DHARD_P.AC.i)
# %%
if "pytest" not in sys.modules:
    lho.display_signal_blockdiagram()
