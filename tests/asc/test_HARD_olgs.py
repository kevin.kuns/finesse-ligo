# %%
import finesse.ligo
import numpy as np
import importlib
import matplotlib.pyplot as plt
import finesse.components.electronics as fce
from finesse_ligo import lho
from finesse.plotting import bode
from finesse.ligo.suspension import QUADSuspension
from finesse.ligo.asc import add_arm_ASC_DOFs
from finesse.ligo.ASC_controllers import get_controller
from finesse.analysis.actions import (
    FrequencyResponse,
    Series,
)
import sys

finesse.init_plotting()
# %%
# create model from aligo
katfile = importlib.resources.read_text(
    "finesse_ligo.katscript", "aligo_reversed_itm.kat"
)
base = finesse.ligo.make_aligo(katscript=katfile)

model = base.deepcopy()
model.modes(maxtem=4)
model.fsig.f = 1
model.L0.P = 54
P_target = 360e3
sol = model.run("run_locks(exception_on_fail=False)")
out = model.run()
sol.plot_error_signals()
print("Power in: " + str(model.L0.P))
print("X arm power: " + str(round(out["Px"]) / 1e3) + " kW")
print("Y arm power: " + str(round(out["Py"]) / 1e3) + " kW")
print("PRG: " + str(round(out["PRG"], 1)))
# %%
# add AS and REFL path
model = lho.add_AS_WFS(model)
model = lho.add_REFL_path(model)

# radius of curvature change at high power
ITM_Rc_D_per_W = -46e-6
ETM_Rc_D_per_W = -33.46e-6

model.ITMXlens.f = model.ITMXlens.f
model.ITMYlens.f = model.ITMYlens.f
model.ITMX.Rc = 2 / (2 / model.ITMX.Rc + ITM_Rc_D_per_W * P_target * 0.5e-6)
model.ITMY.Rc = 2 / (2 / model.ITMY.Rc + ITM_Rc_D_per_W * P_target * 0.5e-6)
model.ETMX.Rc = 2 / (2 / model.ETMX.Rc + ETM_Rc_D_per_W * P_target * 0.5e-6 * 3 / 5)
model.ETMY.Rc = 2 / (2 / model.ETMY.Rc + ETM_Rc_D_per_W * P_target * 0.5e-6 * 3 / 5)

# ASC controllers
DHP_z, DHP_p, DHP_k = get_controller("dhard_p")
DHY_z, DHY_p, DHY_k = get_controller("dhard_y")
CHP_z, CHP_p, CHP_k = get_controller("chard_p")
CHY_z, CHY_p, CHY_k = get_controller("chard_y")

# added scaling factors
model.add(fce.ZPKFilter("DHARD_P_cntrl", DHP_z, DHP_p, 5.3 * DHP_k, gain=1))
model.add(fce.ZPKFilter("DHARD_Y_cntrl", DHY_z, DHY_p, 4.9 * DHY_k, gain=1))
model.add(fce.ZPKFilter("CHARD_P_cntrl", CHP_z, CHP_p, -2.4 * CHP_k, gain=1))
model.add(fce.ZPKFilter("CHARD_Y_cntrl", CHY_z, CHY_p, -16.1 * CHY_k, gain=1))

# %%
sus_component = QUADSuspension

ITMX_sus = model.add(sus_component("ITMX_sus", model.ITMX.mech))
ETMX_sus = model.add(sus_component("ETMX_sus", model.ETMX.mech))
ITMY_sus = model.add(sus_component("ITMY_sus", model.ITMY.mech))
ETMY_sus = model.add(sus_component("ETMY_sus", model.ETMY.mech))

(
    CHARD_P,
    CSOFT_P,
    DHARD_P,
    DSOFT_P,
    CHARD_Y,
    CSOFT_Y,
    DHARD_Y,
    DSOFT_Y,
) = add_arm_ASC_DOFs(model)

# optimize the demod phase
model = lho.optimize_AS_WFS(model)
model = lho.optimize_REFL_WFS(model)

# %%
# sanity check of free sus plants at target power
F_Hz = np.geomspace(0.1, 10, 200)

sol1 = model.run(
    FrequencyResponse(
        F_Hz,
        [DHARD_P.AC.i, CHARD_P.AC.i, DHARD_Y.AC.i, CHARD_Y.AC.i],
        [DHARD_P.AC.o, CHARD_P.AC.o, DHARD_Y.AC.o, CHARD_Y.AC.o],
    )
)

bode(sol1.f, sol1["DHARD_P.AC.i", "DHARD_P.AC.o"], label="DHARD_P", wrap=True)
bode(sol1.f, sol1["CHARD_P.AC.i", "CHARD_P.AC.o"], label="CHARD_P", wrap=True)
bode(sol1.f, sol1["DHARD_Y.AC.i", "DHARD_Y.AC.o"], label="DHARD_Y", wrap=True)
bode(sol1.f, sol1["CHARD_Y.AC.i", "CHARD_Y.AC.o"], label="CHARD_Y", wrap=True)

# # %%
# freq = np.geomspace(1, 10e3, 500)
# darm1 = model.run(FrequencyResponse(freq, model.DARM.AC, model.AS.DC))

# bode(freq, darm1["DARM.AC", "AS.DC"], db=False)
# %%
# create HARD loop error signals and connect
model.connect(model.AS_A_WFS45y.Q, model.DHARD_P_cntrl.p1, name="IN_DH_P")

model.connect(model.REFL_B_WFS9y.I, model.CHARD_P_cntrl.p1, name="IN_CH_P_9B")
model.connect(model.REFL_B_WFS45y.I, model.CHARD_P_cntrl.p1, name="IN_CH_P_45B")
# model.IN_CH_P_9B.gain = 1
# model.IN_CH_P_45B.gain = 1

model.connect(model.AS_A_WFS45x.Q, model.DHARD_Y_cntrl.p1, name="IN_DH_Y")

model.connect(model.REFL_B_WFS9x.I, model.CHARD_Y_cntrl.p1, name="IN_CH_Y_9B")
model.connect(model.REFL_B_WFS45x.I, model.CHARD_Y_cntrl.p1, name="IN_CH_Y_45B")
# model.IN_CH_Y_9B.gain = 1
# model.IN_CH_Y_45B.gain = 1

model.connect(model.DHARD_P_cntrl.p2, DHARD_P.AC.i)
model.connect(model.CHARD_P_cntrl.p2, CHARD_P.AC.i)
model.connect(model.DHARD_Y_cntrl.p2, DHARD_Y.AC.i)
model.connect(model.CHARD_Y_cntrl.p2, CHARD_Y.AC.i)

if "pytest" not in sys.modules:
    model.display_signal_blockdiagram()

# %%
F_Hz = np.geomspace(0.1, 10, 200)


sol = model.run(
    Series(
        FrequencyResponse(
            F_Hz,
            model.DHARD_P_cntrl.p1,
            DHARD_P.AC.o,
            open_loop=True,
            name="DHARD_P_olg",
        ),
        FrequencyResponse(
            F_Hz,
            model.CHARD_P_cntrl.p1,
            CHARD_P.AC.o,
            open_loop=True,
            name="CHARD_P_olg",
        ),
        FrequencyResponse(
            F_Hz,
            model.DHARD_Y_cntrl.p1,
            DHARD_Y.AC.o,
            open_loop=True,
            name="DHARD_Y_olg",
        ),
        FrequencyResponse(
            F_Hz,
            model.CHARD_Y_cntrl.p1,
            CHARD_Y.AC.o,
            open_loop=True,
            name="CHARD_Y_olg",
        ),
    )
)


# %%
if "pytest" not in sys.modules:
    colors = ("xkcd:burgundy", "xkcd:dark olive", "xkcd:salmon pink", "xkcd:cornflower")
    names = ("DHARD_P_olg", "CHARD_P_olg", "DHARD_Y_olg", "CHARD_Y_olg")
    dofs_in = ("DHARD_P.AC.i", "CHARD_P.AC.i", "DHARD_Y.AC.i", "CHARD_Y.AC.i")
    dofs_out = ("DHARD_P.AC.o", "CHARD_P.AC.o", "DHARD_Y.AC.o", "CHARD_Y.AC.o")
    for j in range(4):
        fig, ax = plt.subplots(nrows=2, ncols=1)
        ax[0].semilogx(
            F_Hz,
            20 * np.log10(np.abs(sol[names[j]][dofs_in[j], dofs_out[j]])),
            color=colors[j],
        )
        ax[1].semilogx(
            F_Hz,
            np.angle(sol[names[j]][dofs_in[j], dofs_out[j]], deg=True),
            color=colors[j],
        )
        ax[0].set_title(dofs_in[j][:7])
        ax[0].set_ylabel("Mag")
        ax[1].set_xlabel("Freq [Hz]")
        ax[1].set_ylabel("Phase [deg]")

# # %%
# import warnings

# warnings.filterwarnings("error")
# freq = np.geomspace(2, 10e3, 200)
# darm2 = model.run(FrequencyResponse(freq, model.DARM.AC, model.AS.DC))

# bode(freq, darm2["DARM.AC", "AS.DC"], db=False)
# %%
