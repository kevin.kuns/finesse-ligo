# %%
import finesse
import finesse.ligo
from finesse.ligo.suspension import QUADSuspension
from finesse.analysis.actions import FrequencyResponse
from finesse.components.electronics import ZPKFilter
from finesse.components import DegreeOfFreedom
import sys
import numpy as np
from finesse.plotting import bode
import matplotlib.pyplot as plt

finesse.init_plotting()
# %%
model = finesse.Model()
model.parse(
    """
m ITM
m ETM
link(ITM, 4000, ETM)
fsig(1)
"""
)

ITM = model.ITM
ETM = model.ETM
I_sus = model.add(QUADSuspension("I_sus", ITM.mech))
E_sus = model.add(QUADSuspension("E_sus", ETM.mech))
# Now we make some blend filters to drive either M0 or
# L2 with whatever crossover is needed
w_cross_rad = 2 * np.pi * 1
blend_I_M0 = model.add(ZPKFilter("blend_I_M0", [], [-w_cross_rad], w_cross_rad))
blend_I_L2 = model.add(ZPKFilter("blend_I_L2", [0], [-w_cross_rad], 1))
# Now add the end sus blends
blend_E_M0 = model.add(ZPKFilter("blend_E_M0", [], [-w_cross_rad], w_cross_rad))
blend_E_L2 = model.add(ZPKFilter("blend_E_L2", [0], [-w_cross_rad], 1))
# Now connect the blend outputs to drive the suspensions
model.connect(blend_I_M0.p2.o, I_sus.M0.F_z)
model.connect(blend_I_L2.p2.o, I_sus.L2.F_z)
# and for the ETM...
model.connect(blend_E_M0.p2.o, E_sus.M0.F_z)
model.connect(blend_E_L2.p2.o, E_sus.L2.F_z)

# Make a quick plot to make sure nothing dumb is happening, unity gain, etc.
f = np.geomspace(0.1, 1000, 100)
axs = bode(f, blend_I_M0.eval(f), label="M0")
axs = bode(f, blend_I_L2.eval(f), axs=axs, label="L2")
axs[0].set_title("L2-M0 crossover")

# The process here is we need to define what things to drive
# We first define the gains
ETM_gain = -1
ITM_gain = +1
DARM = model.add(
    DegreeOfFreedom(
        "DARM",
        # Make a new local DOF to describe where to feedback
        # and read out at the suspension/optic
        ITM.phi,  # instead of specifying DOFs we can also specify
        ITM_gain,
        # Then the negative drives to the ETM
        ETM.phi,
        ETM_gain,
    )
)
# Because we haven't connected up the AC parts in the DARM
# DOF above we need to do it ourselves. Note we haven't
# defined AC_IN and AC_OUT above, just some DC actuation
# for the DOF:
#  - connect the DARM input to the two blend filters
#  - connect the optic motion to the output of the DOF so
#    we can compute the total DARM motion if needed
# need to make sure we match up these connections properly
# with the right gains as defined above
model.connect(DARM.AC.i, blend_I_L2.p1.i, gain=ITM_gain)
model.connect(DARM.AC.i, blend_I_M0.p1.i, gain=ITM_gain)
model.connect(ITM.mech.z, DARM.AC.o, gain=ITM_gain)
# now connect up the ETM parts with the opposite -1 gain
model.connect(DARM.AC.i, blend_E_L2.p1.i, gain=ETM_gain)
model.connect(DARM.AC.i, blend_E_M0.p1.i, gain=ETM_gain)
model.connect(ETM.mech.z, DARM.AC.o, gain=ETM_gain)
# The above happens internally when you define AC_IN and
# AC_OUT using LocalDegreeOfFreedom

# %% Show the signal paths
if "pytest" not in sys.modules:
    model.display_signal_blockdiagram()

# %%
f = np.geomspace(0.1, 10, 200)

sol = model.run(
    FrequencyResponse(
        f,
        ["I_sus.gnd.z", "I_sus.M0.F_z", "I_sus.L2.F_z", "DARM.AC.i"],
        ["I_sus.L3.z"],
    )
)
# %%

if "pytest" not in sys.modules:
    plt.loglog(sol.f, abs(sol["I_sus.M0.F_z", "I_sus.L3.z"]), label="M0")
    plt.loglog(sol.f, abs(sol["I_sus.L2.F_z", "I_sus.L3.z"]), label="L2")
    plt.loglog(sol.f, abs(sol["DARM.AC.i", "I_sus.L3.z"]), label="Blended DARM")
    plt.legend()
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Amplitude [m/N]")
