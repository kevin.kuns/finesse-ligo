# %%
import finesse.ligo.lho
from finesse.ligo.actions import InitialLockLIGO

# %%
lho = finesse.ligo.lho.make_O4_lho()
lho.modes("even", maxtem=4)

# %%
# InitialLockLIGO is already an action, we can also print the plan of what that action will do
print(InitialLockLIGO())
print(InitialLockLIGO().plan())
# Using is you just run it directly
sol = lho.run(InitialLockLIGO())
# You could also use it with other actions like, note no `()`
# sol = lho.run(Series(InitialLockLIGO, Noxaxis()))

# %%
print("PRG", sol["after SRC"]["PRG"])
print("PRG9", sol["after SRC"]["PRG9"])
print("PRG45", sol["after SRC"]["PRG45"])
print("9 PRC [W]", sol["after SRC"]["Pprc_9"])
print("45 PRC [W]", sol["after SRC"]["Pprc_45"])
lho.run("run_locks()")
assert sol["after SRC"]["PRG"] > 50
# %%
