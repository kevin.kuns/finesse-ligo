def get_locking_filters(stage, mirror, dof):
    """
    Returns the z, p, k of a stage of a test mass locking filter
    Specify stage name as a string
    Options: L3 (test), L2 (PUM), L1 (UIM) and M0 (top)
    Specify test mass name as a string
    Options: ETMX, ITMX, ETMY, ITMY
    Specify dof, options: P, Y
    (length dof to be added later)
    Current as of Aug 1, 2023
    """
    import numpy as np

    if stage == "L3":
        # L3 locking filters
        L3_z = 1e2 * np.array(
            [
                0 + 1.787541j,
                0 - 1.787541j,
                0 + 1.787541j,
                0 - 1.787541j,
                0 + 1.787541j,
                0 - 1.787541j,
                0 + 1.787541j,
                0 - 1.787541j,
                0 + 1.806347j,
                0 - 1.806347j,
                0 + 1.806347j,
                0 - 1.806347j,
                0 + 1.806347j,
                0 - 1.806347j,
                0 + 1.806347j,
                0 - 1.806347j,
            ]
        )

        L3_p = 1e2 * np.array(
            [
                -0.0036 + 1.7789j,
                -0.0036 - 1.7789j,
                -0.0087 + 1.7839j,
                -0.0087 - 1.7839j,
                -0.0087 + 1.7911j,
                -0.0087 - 1.7911j,
                -0.0060 + 1.7919j,
                -0.0060 - 1.7919j,
                -0.0036 + 1.7963j,
                -0.0036 - 1.7963j,
                -0.0145 + 1.8003j,
                -0.0145 - 1.8003j,
                -0.0146 + 1.8123j,
                -0.0146 - 1.8123j,
                -0.0061 + 1.8209j,
                -0.0061 - 1.8209j,
            ]
        )

        L3_k = 1

        lock_z, lock_p, lock_k = L3_z, L3_p, L3_k

    elif stage == "L2":
        # L2 locking filters
        L2_z = 1e3 * np.array(
            [
                0 + 0.0605j,
                0 - 0.0605j,
                0 - 0.0610j,
                0 + 0.0610j,
                0 - 0.0614j,
                0 + 0.0614j,
                0 - 0.0863j,
                0 + 0.0863j,
                0 - 0.0873j,
                0 + 0.0873j,
                0 - 0.0883j,
                0 + 0.0883j,
                0 - 3.1085j,
                0 + 3.1085j,
                0 - 3.1159j,
                0 + 3.1159j,
                0 - 3.1320j,
                0 + 3.1320j,
                0 - 3.1570j,
                0 + 3.1570j,
                0 - 3.1863j,
                0 + 3.1863j,
                0 - 3.2117j,
                0 + 3.2117j,
                0 - 3.2284j,
                0 + 3.2284j,
                0 + 3.2361j,
                0 - 3.2361j,
            ]
        )

        L2_p = 1e3 * np.array(
            [
                -0.0002 + 0.0601j,
                -0.0002 - 0.0601j,
                -0.0014 - 0.0610j,
                -0.0014 + 0.0610j,
                -0.0002 - 0.0619j,
                -0.0002 + 0.0619j,
                -0.0008 - 0.0845j,
                -0.0008 + 0.0845j,
                -0.0048 - 0.0871j,
                -0.0048 + 0.0871j,
                -0.0008 - 0.0901j,
                -0.0008 + 0.0901j,
                -0.1567 - 2.8644j,
                -0.1567 + 2.8644j,
                -0.0211 - 3.0266j,
                -0.0211 + 3.0266j,
                -0.0061 - 3.0660j,
                -0.0061 + 3.0660j,
                -0.0015 - 3.0779j,
                -0.0015 + 3.0779j,
                -0.0016 - 3.2682j,
                -0.0016 + 3.2682j,
                -0.0065 - 3.2808j,
                -0.0065 + 3.2808j,
                -0.0231 - 3.3234j,
                -0.0231 + 3.3234j,
                -0.1915 + 3.5009j,
                -0.1915 - 3.5009j,
            ]
        )

        L2_k = 1.05

        lock_z, lock_p, lock_k = L2_z, L2_p, L2_k

    elif stage == "L1":
        # L1 locking filters
        L1_z = 0
        L1_p = 0
        L1_k = 1

        lock_z, lock_p, lock_k = L1_z, L1_p, L1_k

    elif stage == "M0":
        # M0 locking filters
        if dof == "P":
            M0_z = np.array([-2.0420 + 2.5505j, -2.0420 - 2.5505j])

            M0_p = np.array(
                [
                    0,
                    -2.1991 + 2.7467j,
                    -2.1991 - 2.7467j,
                    -2.0692 + 3.3746j,
                    -2.0692 - 3.3746j,
                ]
            )

            if mirror == "ETMX":
                M0_k = 0.0031
            elif mirror == "ITMX":
                M0_k = 0.0039
            elif mirror == "ETMY":
                M0_k = 0.0044
            elif mirror == "ITMY":
                M0_k = 0.004

        elif dof == "Y":
            M0_z = []

            M0_p = np.array([0, -2.0692 + 3.3746j, -2.0692 - 3.3746j])

            if mirror == "ETMX":
                M0_k = 0.0141
            elif mirror == "ITMX":
                M0_k = 0.0152
            elif mirror == "ETMY":
                M0_k = 0.0162
            elif mirror == "ITMY":
                M0_k = 0.0155

        lock_z, lock_p, lock_k = M0_z, M0_p, M0_k

    return lock_z, lock_p, lock_k
