# %%


def get_controller(loop):
    """
    Returns the z, p, k of an ASC arm loop controller
    Specify loop name as a string
    Options: chard_y, dhard_y, chard_p, dhard_p, csoft_y, dsoft_y, csoft_p, dsoft_p
    Mostly current for all LHO arm ASC loops as of July 27, 2023
    """
    import numpy as np

    if loop == "chard_y":
        # CHARD Y control
        chard_y_z = 1e3 * np.array(
            [
                -0.000430 + 0.008597j,
                -0.000430 - 0.008597j,
                -0.004241 + 0.016425j,
                -0.004241 - 0.016425j,
                -0.000381 + 0.003796j,
                -0.000381 - 0.003796j,
                -0.000659 + 0.006559j,
                -0.000659 - 0.006559j,
                -0.000861 + 0.008573j,
                -0.000861 - 0.008573j,
                -0.006613 + 0.010685j,
                -0.006613 - 0.010685j,
                -0.002538 + 0.015008j,
                -0.002538 - 0.015008j,
                -0.001627 + 0.016193j,
                -0.001627 - 0.016193j,
                -0.137576 + 0.171833j,
                -0.137576 - 0.171833j,
                -0.060949 + 2.437239j,
                -0.060949 - 2.437239j,
                -0.115092 + 4.602250j,
                -0.115092 - 4.602250j,
            ]
        )
        chard_y_p = 1e3 * np.array(
            [
                -0.006613 + 0.010685j,
                -0.006613 - 0.010685j,
                -0.137576 + 0.171833j,
                -0.137576 - 0.171833j,
                -0.000382 + 0.003800j,
                -0.000382 - 0.003800j,
                -0.000860 + 0.008559j,
                -0.000860 - 0.008559j,
                -0.000430 + 0.008597j,
                -0.000430 - 0.008597j,
                -0.002755 + 0.010652j,
                -0.002755 - 0.010652j,
                -0.004241 + 0.016425j,
                -0.004241 - 0.016425j,
                -0.009956 + 0.039784j,
                -0.009956 - 0.039784j,
                -0.000059 + 0.000000j,
                -0.030846 + 0.000000j,
                -0.459899 + 0.688310j,
                -0.459899 - 0.688310j,
                -0.072089 + 1.295601j,
                -0.072089 - 1.295601j,
            ]
        )

        chard_y_k = 0.362857598404300

        controller_z, controller_p, controller_k = chard_y_z, chard_y_p, chard_y_k

    elif loop == "dhard_y":
        # DHARD Y control
        dhard_y_z = 1e2 * np.array(
            [
                -0.035064 + 0.043795j,
                -0.035064 - 0.043795j,
                -0.026179 + 0.017365j,
                -0.026179 - 0.017365j,
                -0.057406 + 0.078939j,
                -0.057406 - 0.078939j,
                -0.011373 + 0.164115j,
                -0.011373 - 0.164115j,
                -0.738065 + 1.278364j,
                -0.738065 - 1.278364j,
                -1.572770 + 1.964390j,
                -1.572770 - 1.964390j,
                -1.554995 + 2.327304j,
                -1.554995 - 2.327304j,
                -0.000628 + 0.000000j,
            ]
        )

        dhard_y_p = 1e3 * np.array(
            [
                -0.000785 + 0.003041j,
                -0.000785 - 0.003041j,
                -0.058024 + 0.086842j,
                -0.058024 - 0.086842j,
                -0.071527 + 0.107052j,
                -0.071527 - 0.107052j,
                -4.510735 + 4.510735j,
                -4.510735 - 4.510735j,
                -0.000313 + 0.000000j,
                -0.010995 + 0.000000j,
                -0.010995 + 0.000000j,
                -0.113126 + 0.000000j,
                -0.113126 + 0.000000j,
                -1.297606 + 0.000000j,
                -0.137576 + 0.171833j,
                -0.137576 - 0.171833j,
                -0.000062 + 0.000000j,
                -0.633293 + 0.000000j,
            ]
        )

        dhard_y_k = -3.236380146935264e14

        controller_z, controller_p, controller_k = dhard_y_z, dhard_y_p, dhard_y_k

    elif loop == "dhard_p":
        # DHARD P control

        dhard_p_z = 1e2 * np.array(
            [
                -0.006283 + 0.062516j,
                -0.006283 - 0.062516j,
                -0.012042 + 0.144011j,
                -0.012042 - 0.144011j,
                -0.034999 + 0.014999j,
                -0.034999 - 0.014999j,
                -0.314382 + 1.859904j,
                -0.314382 - 1.859904j,
                -5.648610 + 9.783703j,
                -5.648610 - 9.783703j,
            ]
        )

        dhard_p_p = 1e2 * np.array(
            [
                -0.034999 + 0.015000j,
                -0.034999 - 0.015000j,
                -1.375768 + 1.718336j,
                -1.375768 - 1.718336j,
                -0.000628 + 0.000000j,
                -6.332936 + 0.000000j,
                -0.502755 + 0.870798j,
                -0.502755 - 0.870798j,
                -0.457050 + 0.895634j,
                -0.457050 - 0.895634j,
            ]
        )

        dhard_p_k = -5.939696604986611e02

        controller_z, controller_p, controller_k = dhard_p_z, dhard_p_p, dhard_p_k

    elif loop == "chard_p":
        # CHARD P control

        chard_p_z = 1e3 * np.array(
            [
                -0.000628 + 0.006251j,
                -0.000628 - 0.006251j,
                -0.001204 + 0.014401j,
                -0.001204 - 0.014401j,
                -0.003499 + 0.001499j,
                -0.003499 - 0.001499j,
                -0.050569 + 0.087589j,
                -0.050569 - 0.087589j,
                -0.110758 + 0.191839j,
                -0.110758 - 0.191839j,
                -0.060949 + 2.437239j,
                -0.060949 - 2.437239j,
                -0.115092 + 4.602250j,
                -0.115092 - 4.602250j,
            ]
        )

        chard_p_p = 1e3 * np.array(
            [
                -0.137576 + 0.171833j,
                -0.137576 - 0.171833j,
                -0.000062 + 0.000000j,
                -0.633293 + 0.000000j,
                -0.003499 + 0.001500j,
                -0.003499 - 0.001500j,
                -0.018897 + 0.032731j,
                -0.018897 - 0.032731j,
                -0.010465 + 0.061912j,
                -0.010465 - 0.061912j,
                -0.854993 + 0.567139j,
                -0.854993 - 0.567139j,
                -0.129760 + 1.291099j,
                -0.129760 - 1.291099j,
            ]
        )

        chard_p_k = 32.789881687676434

        controller_z, controller_p, controller_k = chard_p_z, chard_p_p, chard_p_k

    elif loop == "csoft_p":
        # CSOFT P
        csoft_p_z = 1e2 * np.array(
            [
                0,
                -0.0213 + 0.0205j,
                -0.0213 - 0.0205j,
                -0.0088 + 0.0875j,
                -0.0088 - 0.0875j,
                -0.0188 + 0.0000j,
                -0.0036 + 0.1960j,
                -0.0036 - 0.1960j,
                -0.0000 + 0.3833j,
                -0.0000 - 0.3833j,
                -0.1605 + 1.2737j,
                -0.1605 - 1.2737j,
                -1.4830 + 2.5686j,
                -1.4830 - 2.5686j,
            ]
        )

        csoft_p_p = np.array(
            [
                -0.6283,
                -0.2676 + 2.9409j,
                -0.2676 - 2.9409j,
                -18.8517 + 73.0126j,
                -18.8517 - 73.0126j,
                -31.4215 + 88.8731j,
                -31.4215 - 88.8731j,
                -2.0280 + 19.4985j,
                -2.0280 - 19.4985j,
                -1.2778 + 38.3072j,
                -1.2778 - 38.3072j,
                -18.8937 + 18.5120j,
                -18.8937 - 18.5120j,
                -4.3217 + 53.9298j,
                -4.3217 - 53.9298j,
            ]
        )

        csoft_p_k = 2.2333e08

        controller_z, controller_p, controller_k = csoft_p_z, csoft_p_p, csoft_p_k

    elif loop == "dsoft_p":
        # DSOFT p
        dsoft_p_z = 1e2 * np.array(
            [
                0,
                -0.0088 + 0.0875j,
                -0.0088 - 0.0875j,
                -0.0188 + 0.0000j,
                -0.0036 + 0.1960j,
                -0.0036 - 0.1960j,
                -0.0000 + 0.3833j,
                -0.0000 - 0.3833j,
                -0.0107 + 0.0275j,
                -0.0107 - 0.0275j,
                -0.1605 + 1.2737j,
                -0.1605 - 1.2737j,
                -1.4830 + 2.5686j,
                -1.4830 - 2.5686j,
            ]
        )

        dsoft_p_p = 1e2 * np.array(
            [
                -0.6283 / 1e2,
                -0.4601 + 1.0002j,
                -0.4601 - 1.0002j,
                -0.3918 + 1.1081j,
                -0.3918 - 1.1081j,
                -0.0203 + 0.1950j,
                -0.0203 - 0.1950j,
                -0.0128 + 0.3831j,
                -0.0128 - 0.3831j,
                -0.0027 + 0.0294j,
                -0.0027 - 0.0294j,
                -0.1102 + 0.2405j,
                -0.1102 - 0.2405j,
                -0.0432 + 0.5393j,
                -0.0432 - 0.5393j,
            ]
        )

        dsoft_p_k = 7.4391e08

        controller_z, controller_p, controller_k = dsoft_p_z, dsoft_p_p, dsoft_p_k

    elif loop == "dsoft_y":
        # DSOFT Y
        dsoft_y_z = 1e2 * np.array(
            [
                0,
                -0.0107 + 0.0275j,
                -0.0107 - 0.0275j,
                -0.0674 + 0.8063j,
                -0.0674 - 0.8063j,
                -0.5907 + 1.6707j,
                -0.5907 - 1.6707j,
                -0.0036 + 0.1960j,
                -0.0036 - 0.1960j,
                -0.0000 + 0.3833j,
                -0.0000 - 0.3833j,
                -0.0082 + 0.0813j,
                -0.0082 - 0.0813j,
                -0.0188 + 0.0000j,
            ]
        )

        dsoft_y_p = 1e2 * np.array(
            [
                -0.6283 / 1e2,
                -0.0027 + 0.0294j,
                -0.0027 - 0.0294j,
                -0.1512 + 0.2618j,
                -0.1512 - 0.2618j,
                -0.4186 + 0.2777j,
                -0.4186 - 0.2777j,
                -0.0203 + 0.1950j,
                -0.0203 - 0.1950j,
                -0.0128 + 0.3831j,
                -0.0128 - 0.3831j,
                -0.4601 + 1.0002j,
                -0.4601 - 1.0002j,
                -0.3918 + 1.1081j,
                -0.3918 - 1.1081j,
            ]
        )

        dsoft_y_k = 2.0462e08

        controller_z, controller_p, controller_k = dsoft_y_z, dsoft_y_p, dsoft_y_k

    elif loop == "csoft_y":
        # CSOFT Y
        csoft_y_z = 1e2 * np.array(
            [
                0,
                -0.0674 + 0.8063j,
                -0.0674 - 0.8063j,
                -0.5907 + 1.6707j,
                -0.5907 - 1.6707j,
                -0.0082 + 0.0813j,
                -0.0082 - 0.0813j,
                -0.0188 + 0.0000j,
                -0.0036 + 0.1960j,
                -0.0036 - 0.1960j,
                -0.0000 + 0.3833j,
                -0.0000 - 0.3833j,
            ]
        )

        csoft_y_p = np.array(
            [
                -0.6283,
                -15.1174 + 26.1841j,
                -15.1174 - 26.1841j,
                -41.8595 + 27.7664j,
                -41.8595 - 27.7664j,
                -18.8517 + 73.0126j,
                -18.8517 - 73.0126j,
                -31.4215 + 88.8731j,
                -31.4215 - 88.8731j,
                -2.0280 + 19.4985j,
                -2.0280 - 19.4985j,
                -1.2778 + 38.3072j,
                -1.2778 - 38.3072j,
            ]
        )

        csoft_y_k = 7.3000e07

        controller_z, controller_p, controller_k = csoft_y_z, csoft_y_p, csoft_y_k

    return controller_z, controller_p, controller_k
