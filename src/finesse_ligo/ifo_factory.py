# %%
import numpy as np
import finesse
import finesse.ligo
import finesse.components as fc
from munch import Munch  # pip install Munch
from finesse.materials import FusedSilica

import importlib
import os


# %%
class ALIGOFactory:
    def __init__(self, parameters):
        if os.path.exists(parameters):
            raise NotImplementedError()
        elif importlib.resources.is_resource(
            "finesse_ligo.parameter_files", parameters
        ):
            self.params = Munch.fromYAML(
                importlib.resources.read_text(
                    "finesse_ligo.parameter_files", parameters
                )
            )
        else:
            raise Exception(f"Could not handle parameters: {parameters}")

    def make(self, *, add_ASC_DOFs=True):
        base = finesse.Model()
        self.pre_make(base, self.params)

        self.ITMX_AR_fr, self.ITMX_HR_bk = self.add_arm_cavity(base, "X", self.params.X)
        self.ITMY_AR_fr, self.ITMY_HR_bk = self.add_arm_cavity(base, "Y", self.params.Y)

        (
            self.BS_X,
            self.BS_Y,
            self.BS_PR,
            self.BS_SR,
            self.BS_HR_X,
            self.BS_HR_Y,
        ) = self.add_BS(base, self.params.BS)

        self.PRM_AR_fr, self.PRM_HR_fr = self.add_PRC(base, self.params.PRC, self.BS_PR)
        self.SRM_AR_fr, self.SRM_HR_fr = self.add_SRC(base, self.params.SRC, self.BS_SR)

        self.add_MICH(base, "X", self.params.X, self.BS_X, self.ITMX_AR_fr)
        self.add_MICH(base, "Y", self.params.Y, self.BS_Y, self.ITMY_AR_fr)

        self.add_OMC(base, self.params.OMC)

        self.add_input_path(base, self.params.input)
        self.add_output_path(base, self.params.output)

        if add_ASC_DOFs:
            self.add_ASC_DOFs(base)

        self.post_make(base, self.params)
        return base

    def pre_make(self, model, params):
        pass

    def add_arm_cavity(self, model, which, params):
        ITM = model.add(
            fc.Mirror(f"ITM{which}"), T=params.ITM.T, L=params.ITM.L, Rc=params.ITM.Rc
        )

        ETM = model.add(
            fc.Mirror(f"ETM{which}"), T=params.ETM.T, L=params.ETM.L, Rc=params.ETM.Rc
        )

        ITMAR = model.add(
            fc.Mirror(
                f"ITM{which}AR",
                T=1,
                L=0,
                xbeta=ITM.xbeta.ref,
                ybeta=ITM.ybeta.ref,
                phi=ITM.phi.ref,
            )
        )

        model.connect(ITM.p1, ETM.p1, name=f"L{which}", L=params.length_arm)
        model.connect(
            ITMAR.p2,
            ITM.p2,
            name=f"subITM{which}",
            L=params.ITM.thickness,
            nr=FusedSilica.nr,
        )

        return ITMAR.p1, ITM.p2

    def add_PRC(self, model, params, BS_port):
        PRM = model.add(
            fc.Mirror("PRM"), T=params.PRM.T, L=params.PRM.L, Rc=params.PRM.Rc
        )

        PRMAR = model.add(
            fc.Mirror(
                "PRMAR",
                T=1,
                L=0,
                Rc=params.PRMAR.Rc,
                xbeta=PRM.xbeta.ref,
                ybeta=PRM.ybeta.ref,
                phi=PRM.phi.ref,
            )
        )

        PR2 = model.add(
            fc.Beamsplitter("PR2"),
            T=params.PR2.T,
            L=params.PR2.L,
            Rc=params.PR2.Rc,
            alpha=params.PR2.AOI,
        )

        PR3 = model.add(
            fc.Beamsplitter("PR3"),
            T=params.PR3.T,
            L=params.PR3.L,
            Rc=params.PR3.Rc,
            alpha=params.PR3.AOI,
        )

        model.connect(PRM.p1, PR2.p1, name="lp1", L=params.length_PRM_PR2)
        model.connect(PR2.p2, PR3.p1, name="lp2", L=params.length_PR2_PR3)
        model.connect(PR3.p2, BS_port, name="lp3", L=params.length_PR3_BS)
        model.connect(
            PRMAR.p2, PRM.p2, name="subPRM", L=params.PRM.thickness, nr=FusedSilica.nr
        )

        return PRMAR.p2, PRM.p1

    def add_SRC(self, model, params, BS_port):
        SRM = model.add(
            fc.Mirror("SRM"), T=params.SRM.T, L=params.SRM.L, Rc=params.SRM.Rc
        )

        SRMAR = model.add(
            fc.Mirror(
                "SRMAR",
                T=1,
                L=0,
                Rc=params.SRMAR.Rc,
                xbeta=SRM.xbeta.ref,
                ybeta=SRM.ybeta.ref,
                phi=SRM.phi.ref,
            )
        )

        SR2 = model.add(
            fc.Beamsplitter("SR2"),
            T=params.SR2.T,
            L=params.SR2.L,
            Rc=params.SR2.Rc,
            alpha=params.SR2.AOI,
        )

        SR3 = model.add(
            fc.Beamsplitter("SR3"),
            T=params.SR3.T,
            L=params.SR3.L,
            Rc=params.SR3.Rc,
            alpha=params.SR3.AOI,
        )

        model.connect(
            SRMAR.p2, SRM.p2, name="subSRM", L=params.SRM.thickness, nr=FusedSilica.nr
        )
        model.connect(SRM.p1, SR2.p1, name="ls1", L=params.length_SRM_SR2)
        model.connect(SR2.p2, SR3.p1, name="ls2", L=params.length_SR2_SR3)
        model.connect(SR3.p2, BS_port, name="ls3", L=params.length_SR3_BS)

        return SRMAR.p1, SRM.p1

    def add_BS(self, model, params):
        BS = model.add(fc.Beamsplitter("BS", T=0.5, R=0.5))
        BSARX = model.add(fc.Mirror("BSARX", T=1, L=0))
        BSARASR = model.add(fc.Mirror("BSARAS", T=1, L=0))

        alpha = np.arcsin(1 / 1.45 * np.sin(np.deg2rad(45)))
        L_BS_sub = params.thickness / np.cos(alpha)

        model.connect(BS.p3, BSARX.p2, L=L_BS_sub, name="subBS_X", nr=FusedSilica.nr)
        model.connect(BS.p4, BSARASR.p2, L=L_BS_sub, name="subBS_SR", nr=FusedSilica.nr)

        return BSARX.p1, BS.p2, BS.p1, BSARASR.p1, BS.p3, BS.p1

    def add_MICH(self, model, which, params, BS_port, ITM_AR_fr_port):
        CP1 = model.add(fc.Mirror(f"CP{which}1", T=1, L=0))
        CP2 = model.add(fc.Mirror(f"CP{which}2", T=1, L=0))
        model.connect(CP1.p2, CP2.p2, name=f"subCP{which}", nr=FusedSilica.nr)
        model.connect(BS_port, CP1.p1, name=f"l{which.lower()}", L=params.length_BS_CP)
        model.connect(
            CP2.p1, ITM_AR_fr_port, name=f"l{which.lower()}2", L=params.length_CP_TM
        )

    def add_input_path(self, model, params):
        if not params.options.simplify_input_path:
            MC1 = model.add(
                fc.Beamsplitter(
                    "MC1",
                    T=params.MC1.T,
                    L=params.MC1.L,
                    Rc=params.MC1.Rc,
                    alpha=params.MC1.AOI,
                )
            )
            MC2 = model.add(
                fc.Beamsplitter(
                    "MC2",
                    T=params.MC2.T,
                    L=params.MC2.L,
                    Rc=params.MC2.Rc,
                    alpha=params.MC2.AOI,
                )
            )
            MC3 = model.add(
                fc.Beamsplitter(
                    "MC3",
                    T=params.MC3.T,
                    L=params.MC3.L,
                    Rc=params.MC3.Rc,
                    alpha=params.MC3.AOI,
                )
            )
            IM1 = model.add(
                fc.Beamsplitter(
                    "IM1",
                    T=params.IM1.T,
                    L=params.IM1.L,
                    Rc=params.IM1.Rc,
                    alpha=params.IM1.AOI,
                )
            )

            model.connect(MC1.p1, MC2.p1, L=params.IMC.length_MC1_MC2)
            model.connect(MC2.p1, MC3.p1, L=params.IMC.length_MC2_MC3)
            model.connect(MC3.p1, MC1.p1, L=params.IMC.length_MC3_MC1)
            model.connect(MC3.p1, MC1.p1, L=params.IMC.length_MC3_MC1)
            model.connect(MC3.p3, IM1.p1, L=params.IMC.length_IMC_IM1)

        IM2 = model.add(
            fc.Beamsplitter(
                "IM2",
                T=params.IM2.T,
                L=params.IM2.L,
                Rc=params.IM2.Rc,
                alpha=params.IM2.AOI,
            )
        )
        IM3 = model.add(
            fc.Beamsplitter(
                "IM3",
                T=params.IM3.T,
                L=params.IM3.L,
                Rc=params.IM3.Rc,
                alpha=params.IM3.AOI,
            )
        )
        IM4 = model.add(
            fc.Beamsplitter(
                "IM4",
                T=params.IM4.T,
                L=params.IM4.L,
                Rc=params.IM4.Rc,
                alpha=params.IM4.AOI,
            )
        )
        _ = (IM2, IM3, IM4)

    def add_output_path(self, model, params):
        pass

    def add_laser(self, model, params):
        pass

    def add_OMC(self, model, params):
        OMC_IC = model.add(
            fc.Beamsplitter("OMC_IC"),
            T=params.IC.T,
            L=params.IC.L,
            Rc=params.IC.Rc,
            alpha=params.IC.AOI,
        )
        OMC_OC = model.add(
            fc.Beamsplitter("OMC_OC"),
            T=params.OC.T,
            L=params.OC.L,
            Rc=params.OC.Rc,
            alpha=params.OC.AOI,
        )
        OMC_CM1 = model.add(
            fc.Beamsplitter("OMC_CM1"),
            T=params.CM1.T,
            L=params.CM1.L,
            Rc=params.CM1.Rc,
            alpha=params.CM1.AOI,
        )
        OMC_CM2 = model.add(
            fc.Beamsplitter("OMC_CM2"),
            T=params.CM2.T,
            L=params.CM2.L,
            Rc=params.CM2.Rc,
            alpha=params.CM2.AOI,
        )

        model.connect(OMC_IC.p3, OMC_OC.p1, L=params.length_IC_OC)
        model.connect(OMC_OC.p2, OMC_CM1.p1, L=params.length_OC_CM1)
        model.connect(OMC_CM1.p2, OMC_CM2.p1, L=params.length_CM1_CM2)
        model.connect(OMC_CM2.p2, OMC_IC.p4, L=params.length_CM2_IC)

        return OMC_IC.p1, OMC_OC.p3

    def add_LSC_DOFs(self, model):
        pass

    def add_LSC_readouts(self, model):
        pass

    def add_ASC_DOFs(self, model):
        pass

    def add_ASC_readouts(self, model):
        pass

    def post_make(self, model, params):
        lx = model.path(self.BS_HR_X.o, self.ITMX_HR_bk.i)
        ly = model.path(self.BS_HR_Y.o, self.ITMY_HR_bk.i)

        model.add(fc.Variable("lschnupp", lx.optical_length - ly.optical_length))

        spx = model.path(self.SRM_HR_fr.o, self.ITMX_HR_bk.i)
        spy = model.path(self.SRM_HR_fr.o, self.ITMY_HR_bk.i)
        ppx = model.path(self.PRM_HR_fr.o, self.ITMX_HR_bk.i)
        ppy = model.path(self.PRM_HR_fr.o, self.ITMY_HR_bk.i)

        model.add(fc.Variable("L_SRC", (spx.optical_length + spy.optical_length) / 2))
        model.add(fc.Variable("L_PRC", (ppx.optical_length + ppy.optical_length) / 2))


factory = ALIGOFactory("lho_O4.yaml")
lho = factory.make()
